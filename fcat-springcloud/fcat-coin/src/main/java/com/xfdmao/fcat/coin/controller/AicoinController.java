package com.xfdmao.fcat.coin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xfdmao.fcat.coin.entity.Aicoin;
import com.xfdmao.fcat.coin.entity.Kline;
import com.xfdmao.fcat.coin.mapper.AicoinMapper;
import com.xfdmao.fcat.coin.service.KlineService;
import com.xfdmao.fcat.common.controller.BaseController;
import com.xfdmao.fcat.common.util.JsonUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by fier on 2018/10/22
 */
@RestController
@RequestMapping("v1/aicoin")
public class AicoinController{
    private static Logger logger = Logger.getLogger(AicoinController.class);

    @Autowired
    private AicoinMapper aicoinMapper;
    @Autowired
    private KlineService klineService;
    @GetMapping(value = "/dealData")
    public JSONObject dealData() {
        List<Aicoin> aicoins = aicoinMapper.selectAll();
        List<Kline> klines = new ArrayList<>();
        for(Aicoin aicoin :aicoins){
            JSONObject jsonObject = JSONObject.parseObject(aicoin.getMsgJson());
            JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("kline_data");
            for(int i=0;i<jsonArray.size();i++){
                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                Kline kline = new Kline();
                kline.setDateId(jsonArray1.getLong(0));
                kline.setOpen(jsonArray1.getDouble(1));
                kline.setHigh(jsonArray1.getDouble(2));
                kline.setLow((jsonArray1.getDouble(3)));
                kline.setClose(jsonArray1.getDouble(4));
                kline.setVol(jsonArray1.getDouble(5));
                kline.setKlineDate(new Date(kline.getDateId()*1000));

                kline.setContractType("quarter");
                kline.setPeriod("15min");
                kline.setSymbol(aicoin.getSymbol());
                klines.add(kline);
            }
        }
        klineService.insertBatch(klines);
        return JsonUtil.getSuccessJsonObject();
    }

}
