package com.xfdmao.fcat.coin.entity;

import javax.persistence.*;

public class Aicoin {
    @Id
    private Long id;

    private String symbol;

    private String remark;

    @Column(name = "msg_json")
    private String msgJson;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return msg_json
     */
    public String getMsgJson() {
        return msgJson;
    }

    /**
     * @param msgJson
     */
    public void setMsgJson(String msgJson) {
        this.msgJson = msgJson;
    }
}