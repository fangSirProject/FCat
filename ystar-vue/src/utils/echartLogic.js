//净收益
export function netIncomeLogic(item) {
	
	item = JSON.parse(item)
	item.reverse();//数组倒排
	
	//echart数据
	let netIncomeData = {
		date: [],
		incomeRate: []
	};
	let index = 0;
	
	//echart数据 处理
	item.forEach(data=>{
		if(index == 0){
			netIncomeData.date[index] = data.date.slice(0,10);
			netIncomeData.incomeRate[index] = Number((data.incomeRate*100).toFixed(4));
			index = index + 1;
		}else {
			if(netIncomeData.date[index-1] == data.date.slice(0,10)){
				netIncomeData.incomeRate[index-1] = (Number((data.incomeRate*100).toFixed(4)) + Number(netIncomeData.incomeRate[index-1])).toFixed(4);
			}else {
				netIncomeData.date[index] = data.date.slice(0,10);
				netIncomeData.incomeRate[index] = Number((data.incomeRate * 100).toFixed(4))
				index = index + 1;
			}
		};
	});
	
	return netIncomeData
}

//累计收益
export function cumulativeIncomeLogic(item) {
	
	item = JSON.parse(item)
	item.reverse();//数组倒排
	
	//echart数据
	let cumulativeIncomeData = {
		date: [],
		incomeRate: []
	};
	let index = 0;
	
	//echart数据 处理
	item.forEach(data=>{
		if(index == 0){
			cumulativeIncomeData.date[index] = data.date.slice(0,10);
			cumulativeIncomeData.incomeRate[index] = Number((data.incomeRate*100).toFixed(4));
			index = index + 1;
		}else {
			if(cumulativeIncomeData.date[index-1] == data.date.slice(0,10)){
				cumulativeIncomeData.incomeRate[index-1] = (Number((data.incomeRate*100).toFixed(4)) + Number(cumulativeIncomeData.incomeRate[index-1])).toFixed(4);
			}else {
				cumulativeIncomeData.date[index] = data.date.slice(0,10);
				cumulativeIncomeData.incomeRate[index] = (Number((data.incomeRate * 100).toFixed(4)) + Number(cumulativeIncomeData.incomeRate[index-1])).toFixed(4);
				index = index + 1;
			}
		};
	});
	
	return cumulativeIncomeData
}


//K线图
export function klinkPicLogic(item) {
	//echart数据
	let kLinesData = [];
	
	//开盘、收盘、最低价、最高价、买卖数、涨幅、上引线跌幅、下引线涨幅、量比
	item.forEach((data, index)=>{
		let dayData = [];
		dayData[0] = data.date.slice(0,16);
		dayData[1] = data.open;
		dayData[2] = data.close;
		dayData[3] = data.low;
		dayData[4] = data.high;
		dayData[5] = data.volume;
		dayData[6] = (data.gain*100).toFixed(2);
		dayData[7] = (data.upLeadGain*100).toFixed(2);
		dayData[8] = (data.downLeadGain*100).toFixed(2);
		dayData[9] = (data.volRate).toFixed(2);
		kLinesData[index] = dayData;
	});
	
	return kLinesData
}

//K线图 - 买入卖出标记数组
export function getBuySellList(item) {
	var buySellList = [];
	var buySellStatus = '';
	
	//开盘、收盘、最低价、最高价、买卖数
	item.forEach(data=>{
		if(data.buySellStatus != ""){
			switch (data.buySellStatus){
				case 'sell':
					buySellStatus = '卖';
					break;
				case 'buy':
					buySellStatus = '买';
					break;
				case "SELL_OPEN":
		            buySellStatus = "开空";
		            break;
	          	case "BUY_CLOSE":
		            buySellStatus = "平空";
		            break;
			};
			if(buySellStatus == '买' || buySellStatus == '平空'){
				var buySell = {
			        value: buySellStatus,
			        xAxis: data.date.slice(0,16),
			        yAxis: (data.high)*1.0007,
			        itemStyle : {
    		            color: '#00da3c'
    		        }
			    };
			}else {
				var buySell = {
			        value: buySellStatus,
			        xAxis: data.date.slice(0,16),
			        yAxis: (data.low)*0.9993,
			        symbolRotate: 180,
			        label: {
			            offset: [0,10]
			        },
			        itemStyle : {
    		            color: '#ec0000'
    		        }
			    };
			};
		    buySellList.push(buySell); 
		};
	});
	
	return buySellList
}